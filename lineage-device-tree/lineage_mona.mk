#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from mona device
$(call inherit-product, device/xiaomi/mona/device.mk)

PRODUCT_DEVICE := mona
PRODUCT_NAME := lineage_mona
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := 2109119BC
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="mona-user 12 RKQ1.211001.001 V13.0.5.0.SKVCNXM release-keys"

BUILD_FINGERPRINT := Xiaomi/mona/mona:12/RKQ1.211001.001/V13.0.5.0.SKVCNXM:user/release-keys
